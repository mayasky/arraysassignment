package main;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        //example 1        
        int arr1[] = {1, 2, 2, 4};
        int arr2[] = {1, 2, 2, 3, 4, 4};

        printIntersection(arr1, arr2);

        //example 2
        int arr3[] = {1, 2, 6, 4};
        int arr4[] = {10, 20, 20, 60};

        printIntersection(arr3, arr4);

    }

    static void printIntersection(int array1[], int array2[]) {

        HashSet<Integer> set1 = new HashSet<>();
        HashSet<Integer> set2 = new HashSet<>();

        for (int value : array1) {
            set1.add(value);
        }

        for (int value : array2) {
            if (set1.contains(value)) {
                set2.add(value);
            }
        }

        if (!set2.isEmpty()) {
            System.out.println(set2.toString());
        } else {
            System.out.println(-1);
        }

    }

}
